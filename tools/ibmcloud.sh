#!/bin/bash
#https://cloud.ibm.com/iam/apikeys
#https://cloud.ibm.com/docs/vpc-on-classic-vsi?topic=vpc-on-classic-vsi-profiles
#https://cloud.ibm.com/docs/vpc-on-classic?topic=vpc-on-classic-creating-a-vpc-using-the-rest-apis
#https://cloud.ibm.com/apidocs/vpc-on-classic

export rias_endpoint="https://eu-de.iaas.cloud.ibm.com"
#export apikey="<apikey>"
#curl -k -X POST --header "Content-Type: application/x-www-form-urlencoded" --header "Accept: application/json" --data-urlencode "grant_type=urn:ibm:params:oauth:grant-type:apikey" --data-urlencode "apikey=$apikey" "https://iam.cloud.ibm.com/identity/token" | python -m json.tool
TOKEN=$(curl -sk -X POST --header "Content-Type: application/x-www-form-urlencoded" --header "Accept: application/json" --data-urlencode "grant_type=urn:ibm:params:oauth:grant-type:apikey" --data-urlencode "apikey=$apikey" "https://iam.cloud.ibm.com/identity/token" | python -m json.tool | awk '$1 ~ /access_token/ { print $NF }' | sed -e 's/\"//g' -e 's/,//g')
#export iam_token="Bearer <token>"
export iam_token="Bearer ${TOKEN}"

#export version="2019-12-14"
export version="$(date -d '1 day ago' '+%Y-%m-%d')"
export generation=1

METHOD=$1
urlpath=$2
PAYLOAD=$3

if [ "${METHOD}" == "help" ] || [ "${METHOD}" == "--help" ] || [ "${METHOD}" == "-h" ]
then
  cat <<EOF
$(basename $0) GET /keys
$(basename $0) GET /regions
$(basename $0) GET /vpcs
$(basename $0) GET /images
$(basename $0) GET /instances
$(basename $0) POST /instances/fe968c7f-65a4-44e3-8199-b7165f57c880/actions '{"type": "stop"}'
$(basename $0) POST /instances/fe968c7f-65a4-44e3-8199-b7165f57c880/actions '{"type": "start"}'
$(basename $0) POST /instances '{"name":"vps01","profile":{"name":"cc1-2x4"},"zone":{"name":"eu-de-1"},"vpc":{"id":"9d00b071-1706-468b-ba53-abd0e8ac9359"},"image":{"id":"cc8debe0-1b30-6e37-2e13-744bfb2a0c11"},"primary_network_interface":{"subnet":{"id":"7dd76bee-d563-405d-90a8-06b95945611a"}},"keys":[{"id":"e953d55d-0000-0000-0000-000000190502"}]}'
$(basename $0) POST /floating_ips '{"name":"vps01-floatingip-01","target":{"id":"66bb7e97-2e81-4399-9de7-39e4fbfad42b"}}'
$(basename $0) DELETE /instances/41d4bfc1-ae79-4e4a-8ddc-eafe2bd93e3d
EOF
exit 99
elif [ "${METHOD}" == "POST" ]
then
  curl -s -X $METHOD -H "Authorization: $iam_token" "$rias_endpoint/v1${urlpath}?version=$version&generation=$generation" -d "${PAYLOAD}" | python -m json.tool
else
  curl -s -X $METHOD -H "Authorization: $iam_token" "$rias_endpoint/v1${urlpath}?version=$version&generation=$generation" | python -m json.tool
fi
