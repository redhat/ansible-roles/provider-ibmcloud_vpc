# Provider for IBM Cloud VPC

This Ansible role provider is intended to assist with the creation of resources in IBM Cloud VPC. It aims to provide an easy way to automate around IBM Cloud.

## Prerequisites

To be able to provision anything with this role there is a need to create an API key. 
- Go to https://cloud.ibm.com/iam/apikeys
- Click on "Create an IBM Cloud API Key"

The role also requires an SSH key uploaded to the IBM Cloud which will be inserted into new machines.
- Go to https://cloud.ibm.com/vpc/compute/sshKeys
- Click on "Add SSH key"

## How the role works

Essentially the following will happen:
* The role will act on any hosts in the 'new_hosts' or 'delete_hosts' hostgroup, and act accordingly.
* The role will look for the variable provider to be set to the provider name (i.e. ibmcloud_vpc).
* The role will need to know which VPC your host should exist within via the ibmcloud_vpc variable. This will be created if it doesn't exist.
* The role will default to the first subnet existing (or create one if it doesn't exist). Unless specified with the vps_subnet variable.
* The role will create a Floating IP (External IP that you can connect to) if the variable external is set
* The role will insert the provided SSH key into the VM so you can connect as root after it is provisioned

## How to use the role

You can find an example playbook inside the role directory which details how you can use the role.

- Create an Ansible inventory, which should container information similar to this
  ```
  [new_hosts:vars]
  ibmcloud_vpc=vpc01
  ibmcloud_vpc_sshkey="IBMCloudSSHKey"

  [new_hosts]
  bastion.example.com provider=ibmcloud_vpc external=true
  #node[01:03].example.com provider=ibmcloud_vpc ibmcloud_vpc=vpc02 ibmcloud_ssh_key="MySpecialSSHKey" vps_subnet="vpc02net01"

  [delete_hosts]
  #bastion.example.com provider=ibmcloud_vpc vpc=rydevpc
  ```

- Copy your IBM Cloud API Key into an Ansible vault
  ```
  $ ansible-vault create ~/vault.yml
      vault_ibmcloud_apikey: your-long-apikey
  ```

- Execute ansible-playbook
  ```
  $ ansible-playbook -i inventory playbook.yml -e '@~/vault.yml' --ask-vault-pass
  ```

## Variables that can be configured

Variable | Default value | What it is used for
--- | --- | ---
provider | ibmcloud_vpc | Role will only execute if it is set to this value on a host
ibmcloud_vpc | vpc01 | The name of the IBM Cloud VPC we will use
ibmcloud_vpc_sshkey | vpc01sshkey | The name of the SSH key that will be added to newly provisioned machines
ibmcloud_vpc_generation | 1 | Required to communicate with the IBM Cloud API
ibmcloud_vpc_region | eu-de | IBM Cloud region the VPC exists within
ibmcloud_apikey | Fetched from vault as: vault_ibmcloud_apikey | Encrypted value of the IBM Cloud API Key
ibmcloud_api_version | Yesterdays date | Required to communicate with the IBM Cloud API. For production use, should be set to a stable value.
vps_name | The inventory hostname in short form | This will become the instance name
vps_image_name | centos-7.x-amd64 | Which OS image to use. For example, Red Hat image is: red-7.x-amd64
vps_profile | cc1-2x4 | The profile (CPU/Memory/Disk/Network) that will be used
external | false | If a host is seen as external facing, it will get a floating IP assigned
